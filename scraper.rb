require 'nokogiri'
require 'open-uri'
require 'net/http'
require 'pry'

url = ARGV.first
uri = URI.parse(url)

response = Net::HTTP.get_response(uri)
html = response.body

response = Nokogiri::HTML(html)

title = response.css('.banner-wrapper h2').text
ingredients_wrapper = response.css('.ingredients-wrapper')
preparation_wrapper = response.css('.group-recipe-preparation')
recipe_size = ingredients_wrapper.at("h2").text.sub("Ingrediënten voor ", '')

recipe = ""

recipe << %{---
layout: recipe
title: #{title}
show: true
source: EVA
source_url: #{url}
duration: ""
tags: [veggi]
size: #{recipe_size}
---
}
recipe << "\n\n"

recipe << "### #{ingredients_wrapper.at("h2").text}\n\n"
ingredients_wrapper.children.css('.item-list li').each { |c| recipe << "* #{c.text}\n" }

recipe << "\n"
recipe << "### Bereiding\n\n"
preparation_wrapper.css('.field-name-field-recipe-block-text .field-items .field-item').children.each { |c| recipe << "#{c.text}  " }

file_name = "#{Time.now.strftime("%Y-%m-%d")}-#{title.downcase.gsub(/\ |\_/, '-')}.md"

file = File.new("_recipes/#{file_name}", "wb")
file.write(recipe)
file.close

puts file_name
