## EVA recipe scraper

Just an easy way to save recipes from https://www.evavzw.be/recepten in a jekyll-like format.

For example https://www.evavzw.be/recept/prei-champignonpasteitjes will be converted to https://joren.gent/recipes/2021-02-24-prei-champignonpasteitjes/

Just run it like this:

`bundle exec ruby scraper.rb https://www.evavzw.be/recept/prei-champignonpasteitjes`
